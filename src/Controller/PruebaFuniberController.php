<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PruebaFuniberController extends AbstractController
{
    /**
     * @Route("/prueba/funiber", name="prueba_funiber")
     */
    public function index(): Response
    {
        return $this->render('prueba_funiber/index.html.twig', [
            'controller_name' => 'PruebaFuniberController',
        ]);
    }

     public function test()
    {
        return new Response(
            '<html><body>Hello world! Probando Sonido!</body></html>'
        );
    }

    public function Animales(){
        $animales = array('Rana' => 'brr birip brrah croac',
                          'Libelula' => 'fiu plop pep',
                          'Grillo' => 'cric-cric trri-trri bri-bri',
        );

        return new Response(json_encode($animales));
    }
    
    public function busqueda($condicion,$sonido){
        
        switch ($sonido) {
            case 'brr':
                $respuesta = "fiu cric-cric brrah";
                return $respuesta;
                break;
            case 'fiu':
                $respuesta = "cric-cric brrah";
                return $respuesta;
                break;
            case 'cric-cric':
                $respuesta = "brrah";
                return $respuesta;
                break;
            case 'Pep':
                $respuesta = "birip trri-trri croac";
                return $respuesta;
                break;
            case 'birip':
                $respuesta = "trri-trri croac";
                return $respuesta;
                break;
            case 'trri-trri':
                $respuesta = "birip trri-trri croac";
                return $respuesta;
                break;
            case 'bri-bri':
                $respuesta = "plop cric-cric brrah";
                return $respuesta;
                break;
            case 'plop':
                $respuesta = "cric-cric brrah";
                return $respuesta;
                break;
            
        }
        
    }
    public function sonidoSelect(Request $request)
    {
        $Sonido = $request->query->get('Sonido');
        
        if ($Sonido== "croac" || $Sonido=="brrah")  {
            $condicion = True;
            $respuesta="No hay Sonido que mostrar";
        } else {
            $condicion = false;
            $respuesta = $this->busqueda($condicion,$Sonido);
            //$respuesta= "El sonido es:".$respuesta;

            
        }
       
        return $this->render ('prueba_funiber/sonido.html.twig',[
            'mensaje'=>$respuesta,
        ]);
    }

}
